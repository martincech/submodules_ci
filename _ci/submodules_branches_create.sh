#!/bin/bash -e
# creates new branch for each submodule with the same name as top repo branch
# and switches all the submodules to the newly created branch
branch=$(git branch | grep \* | cut -d ' ' -f2)
subrepos=$(git config --file .gitmodules --get-regexp path | awk '{ print $2 }')

git submodule foreach --recursive "git checkout"
git submodule foreach --recursive "git checkout -B $branch"
git submodule foreach --recursive "git push --set-upstream origin $branch"

for subrepo in ${subrepos} ; do
   git config -f .gitmodules submodule.$subrepo.branch $branch   
done

git push --set-upstream origin $branch